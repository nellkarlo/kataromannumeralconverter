using System;
using Xunit;
using RomanNumeralConverter;

namespace RomanNumeralConverterTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(1, "I")]
        [InlineData(1 + 1, "II")]
        [InlineData(1 + 1 + 1, "III")]
        [InlineData(5 - 1, "IV")]
        [InlineData(5, "V")]
        [InlineData(5 + 1 , "VI")]
        [InlineData(5 + 1 + 1, "VII")]
        [InlineData(10 - 1, "IX")]
        [InlineData(10, "X")]
        [InlineData(10 + 1, "XI")]
        [InlineData(10 + 1 + 1, "XII")]
        [InlineData(10 + 1 + 1 +1, "XIII")]
        [InlineData(10 - 1 + 5, "XIV")]
        [InlineData(10 + 5, "XV")]
        [InlineData(-10 + 50 + 5 + 1 + 1 + 1, "XLVIII")]
        [InlineData(-10 + 50 + 5 + 1 + 1, "XLVII")]
        [InlineData(10 + 10 + 10 + 5 + 1 + 1 + 1, "XXXVIII")]
        [InlineData(10 + 10 + 5 + 1 + 1 + 1, "XXVIII")]
        [InlineData(50 + 10 + 5 + 1 + 1 + 1, "LXVIII")]
        [InlineData(50 + 10 + 10 + 10 + 5 + 1 + 1 + 1, "LXXXVIII")]
        [InlineData(100 - 10 - 1 + 5, "XCIV")]
        [InlineData(50 + 10 - 1 + 10, "LXIX")]
        [InlineData(500 + 100 + 100 + 100, "DCCC")]
        [InlineData(1000 - 100, "CM")]
        public void RomanNumeralConverter_GivenAStringOfRomanNumerals_IntegerTotal(int expected, string roman)
        {
            //Arrange
            int actual;
            //Act
            actual = Program.RomanNumeralConverter(roman);
            //Assert
            Assert.Equal(expected, actual);

        }
    }
}
